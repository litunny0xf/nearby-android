package com.play.planet.nearbydemo

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.nearby.Nearby
import com.google.android.gms.nearby.messages.Message
import com.google.android.gms.nearby.messages.MessageListener
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private lateinit var mMessageListener : MessageListener
    private lateinit var mMessage : Message
    private val TAG : String = this::class.java.simpleName

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mMessageListener = object : MessageListener() {
            override fun onFound(message: Message) {
                Log.d(TAG, "Found message: " + String(message.content))
                Toast.makeText(this@MainActivity, String(message.content), Toast.LENGTH_LONG).show()
                hello.text = String(message.content)
            }

            override fun onLost(message: Message) {
                Log.d(TAG, "Lost sight of message: " + String(message.content))
                Toast.makeText(this@MainActivity, String(message.content), Toast.LENGTH_LONG).show()
                hello.text = String(message.content)
            }
        }

        send.setOnClickListener {
            mMessage = Message(editText.text.toString().toByteArray())
            Nearby.getMessagesClient(this).publish(mMessage);
        }

        restart.setOnClickListener {
            onStop()
            onStart()
        }

        mMessage = Message("Yo! This is from Android".toByteArray())
    }

    override fun onStart() {
        super.onStart()
        Nearby.getMessagesClient(this).publish(mMessage);
        Nearby.getMessagesClient(this).subscribe(mMessageListener);
    }

    override fun onStop() {
        Nearby.getMessagesClient(this).unpublish(mMessage);
        Nearby.getMessagesClient(this).unsubscribe(mMessageListener);
        super.onStop()
    }
}